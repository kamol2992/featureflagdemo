﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.FeatureManagement.FeatureFilters;

namespace FaetureFlagsDemo
{
    public class HttpTargetingContextAccessor:ITargetingContextAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpTargetingContextAccessor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        
        public ValueTask<TargetingContext> GetContextAsync()
        {
            //need to cache
            TargetingContext targetingContext = new TargetingContext()
            {
                UserId = "MyCompany1"
            };

            return new ValueTask<TargetingContext>(targetingContext);
        }
    }
}