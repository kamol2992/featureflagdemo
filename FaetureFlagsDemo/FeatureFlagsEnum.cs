﻿namespace FaetureFlagsDemo
{
    public enum FeatureFlagsEnum
    {
        Payments,
        PaymentList,
        PaymentDetail,
        PaymentCreate,
        PaymentEdit,
        PaymentPrint,
    }
}