using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement;
using Microsoft.FeatureManagement.Mvc;

namespace FaetureFlagsDemo.Controllers
{
    [FeatureGate(nameof(FeatureFlagsEnum.Payments))]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IFeatureManager _featureManager;

        public PaymentController(IFeatureManager featureManager)
        {
            _featureManager = featureManager;
        }
        
        [FeatureGate(nameof(FeatureFlagsEnum.PaymentList))]
        [HttpGet("list")]
        public List<string> List()
        {
            return new List<string>()
            {
                "payment1",
                "payment2",
                "payment3",
            };
        }
        
        [HttpGet("create")]
        public async Task<string> Create()
        {
            if (await _featureManager.IsEnabledAsync(nameof(FeatureFlagsEnum.PaymentCreate)))
            {
                return "Create payment is possible.";
            }

            return "Create payment is impossible!";
        }
        
          
        [FeatureGate(nameof(FeatureFlagsEnum.PaymentDetail))]
        [HttpGet("detail")]
        public string Detail()
        {
            return "Detail information";
        }
        
        [FeatureGate(nameof(FeatureFlagsEnum.PaymentEdit))]
        [HttpGet("Edit")]
        public string Edit()
        {
            return "Edit information";
        }
        
        [FeatureGate(nameof(FeatureFlagsEnum.PaymentPrint))]
        [HttpGet("Print")]
        public string Print()
        {
            return "Print information";
        }
        
        [HttpGet("feature-list")]
        public async Task<Dictionary<string, bool>> FeatureList()
        {
            var listOfFeatures = new Dictionary<string, bool>();
            var list =  _featureManager.GetFeatureNamesAsync();

            await foreach (var item in list)
            {
                var value =await _featureManager.IsEnabledAsync(item);
                listOfFeatures[item] = value;
            }

            return listOfFeatures;
        }
    }
}