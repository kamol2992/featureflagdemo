﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.FeatureManagement.Mvc;

namespace FaetureFlagsDemo
{
    public class CustomDisabledFeatureHandler : IDisabledFeaturesHandler
    {
        public Task HandleDisabledFeatures(IEnumerable<string> features, ActionExecutingContext context)
        {
            context.Result = new ContentResult()
            {
                ContentType = "application/json",
                StatusCode = 200,
                Content = "This feature is not available please try again later - " + String.Join(',', features)
            };
            
            return  Task.CompletedTask;
        }
    }
}